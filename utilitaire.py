from math import *
import csv
import sys
import os
import psutil

from technique import *

CHEMIN_FICHIER_VEHI = 'data/generations/donnees_vehicules_'
CHEMIN_FICHIER_ROUES = 'data/generations/donnees_roues_'

def rotation(x, y, autour_x, autour_y, angle):
  x -= autour_x
  y -= autour_y

  xnew = x * cos(angle) - y * sin(angle)
  ynew = x * sin(angle) + y * cos(angle)

  x = xnew + autour_x
  y = ynew + autour_y
  return x,y

### DEF LECTURE = num_file vaut 0 si on n'ouvre pas de fichier, sinon est supérieur
def lecture(tab_v, num_file):
  taille = -1
  doit_lire = False
  num_gen = 0

  if(num_file > 0):   #Un argument : lit le fichier numéro "argument" s'il existe, sinon génération aléatoire
    try:
      nom_fichier_vehi = CHEMIN_FICHIER_VEHI + str(num_file) + '.csv'
      taille = os.path.getsize(nom_fichier_vehi)
      num_gen = num_file
    except Exception as ex:
      print('YOU PICKED THE WRONG FILE FOOL !')
      print(ex)
      print('Pas grave, génération aléatoire')
    if(taille > 0):
      nom_fichier_roues = CHEMIN_FICHIER_ROUES + str(num_file) + '.csv'
      doit_lire = True
    else:
      print('Fichier vide, génération aléatoire')

  else:
    print("Génération aléatoire")
  
  if(doit_lire):
    i = 0
    '''Lecture vehicules'''
    with open(nom_fichier_vehi, 'r') as le_fichier:
      lecteur = csv.reader(le_fichier)
      next(lecteur)
      for ligne in lecteur:
        tab_v[i].fitness = float(ligne[0])
        tab_v[i].dimensions_chassis = [float(ligne[1]), float(ligne[2]), float(ligne[3])]
        tab_v[i].masse = float(ligne[4])
        tab_v[i].angle_max = int(ligne[5])
        tab_v[i].engine_force = int(ligne[6])
        tab_v[i].nombre_roues = int(ligne[7])
        tab_v[i].tableau_roues.clear()    #Préparer le terrain pour la lecture des roues
        i = i+1
    le_fichier.close()

    '''Lecture roues'''
    num_vehi = 0
    r = roue([0, 0, 0], 1)

    with open(nom_fichier_roues, 'r') as lautre_fichier:
      lecteur = csv.reader(lautre_fichier)
      next(lecteur)
      for ligne in lecteur:
        if(int(ligne[0]) != num_vehi):
          num_vehi = num_vehi + 1
        r.position = [float(ligne[1]), float(ligne[2]), float(ligne[3])]
        r.rayon = float(ligne[4])
        r.friction = int(ligne[5])
        r.peut_tourner = bool(ligne[6])
        tab_v[num_vehi].tableau_roues.append(r)
    lautre_fichier.close()
  return num_gen



def ecriture(tab_v, nom_fichier_vehi, nom_fichier_roues):
  '''Ecriture vehicules'''
  with open(nom_fichier_vehi, 'w') as le_fichier:
    le_fichier.truncate()
    ecrivain = csv.writer(le_fichier)
    ecrivain.writerow(('Fitness', 'DimensionX', 'DimensionY', 'DimensionZ', 'Masse', 'Angle max', 'Engine force', 'Nombre de roues'))
    for i in range (len(tab_v)):
      v = tab_v[i]
      ecrivain.writerow((v.fitness, v.dimensions_chassis[0], v.dimensions_chassis[1], v.dimensions_chassis[2], v.masse, v.angle_max, v.engine_force, v.nombre_roues))
  le_fichier.close()

  '''Ecriture roues'''
  with open(nom_fichier_roues, 'w') as lautre_fichier:
    lautre_fichier.truncate()
    ecrivain = csv.writer(lautre_fichier)
    ecrivain.writerow(('Numero vehicule', 'PositionX', 'PositionY', 'PositionZ', 'Rayon', 'Friction', 'PeutTourner'))
    for i in range(len(tab_v)):
      for j in range (tab_v[i].nombre_roues):
        r = tab_v[i].tableau_roues[j]
        ecrivain.writerow((i, r.position[0], r.position[1], r.position[2], r.rayon, r.friction, r.peut_tourner))
  lautre_fichier.close()

def gestion_args(threading, displaying, population, opening):
  threading = False
  displaying = True
  population = 20
  opening = -1
  
  nb_args = len(sys.argv)
  for i in range(1, nb_args):
    if(sys.argv[i] == "--threads"):
      print("On va faire des threads.")
      threading = True
      displaying = False
    elif(sys.argv[i] == "--nodisplay"):
      print("On ne verra pas nos monstruosités, dommage")
      displaying = False
    elif(sys.argv[i] == "--pop"):
      try:
        population = int(sys.argv[i+1])
        print("La population est à ", population)
      except Exception as ex:
        print("La population est erronée")
        print(ex)
    elif(sys.argv[i] == "--open"):
      try:
        opening = int(sys.argv[i+1])
        print("Ouverture du fichier", opening)
      except Exception as ex:
        print("Le numéro du fichier est erroné")
        print(ex)
  return [threading, displaying, population, opening]