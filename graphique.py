
#from pandac.PandaModules import loadPrcFileData
#loadPrcFileData('', 'load-display tinydisplay')
import sys
import direct.directbase.DirectStart
import os
import gc

from direct.showbase.DirectObject import DirectObject
from direct.showbase.InputStateGlobal import inputState

from direct.task import Task

from panda3d.core import AmbientLight
from panda3d.core import DirectionalLight
from panda3d.core import Vec3
from panda3d.core import Vec4
from panda3d.core import Point3
from panda3d.core import TransformState
from panda3d.core import BitMask32
from panda3d.core import Filename
from panda3d.core import PNMImage
from panda3d.core import GeoMipTerrain
from panda3d.core import ConfigVariableString
from panda3d.core import ClockObject

from panda3d.bullet import BulletWorld
from panda3d.bullet import BulletPlaneShape
from panda3d.bullet import BulletBoxShape
from panda3d.bullet import BulletCylinderShape
from panda3d.bullet import BulletVehicle
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletDebugNode
from panda3d.bullet import BulletHeightfieldShape
from panda3d.bullet import ZUp

from threading import Thread, RLock

from technique import *
from utilitaire import *

groupes_coll = ConfigVariableString('bullet-filter-algorithm')
groupes_coll.setValue('groups-mask')

TAUX_RAFRAICHISSEMENT = 60.0
DUREE_SIMULATION = 15
NB_STEPS = TAUX_RAFRAICHISSEMENT * DUREE_SIMULATION
SAUVEGARDES = 1

import gc

def dump_garbage():
    """
    show us what's the garbage about
    """
        
    # force collection
    print ("\nGARBAGE:")
    gc.collect()

    print ("\nGARBAGE OBJECTS:")
    for x in gc.garbage:
        s = str(x)
        if len(s) > 80: s = s[:80]
        print (type(x),"\n  ", s)
'''
if __name__=="__main__":
    import gc
    gc.enable()
    gc.set_debug(gc.DEBUG_LEAK)

    # make a leak
    l = []
    l.append(l)
    del l

    # show the dirt ;-)
    dump_garbage()
'''

class Game(DirectObject):

  def __init__(self):
    self.npV = []
    
    base.setBackgroundColor(0.1, 0.1, 0.8, 1)
    base.setFrameRateMeter(True)
    #base.disableMouse()

    base.cam.setPos(-60, -60, 35)
    base.cam.lookAt(0, 0, 0)
    self.regarder_vehi = True

    #self.vehicle = []
    # Light
    alight = AmbientLight('ambientLight')
    alight.setColor(Vec4(0.5, 0.5, 0.5, 1))
    alightNP = render.attachNewNode(alight)

    dlight = DirectionalLight('directionalLight')
    dlight.setDirection(Vec3(1, 1, -1))
    dlight.setColor(Vec4(0.7, 0.7, 0.7, 1))
    dlightNP = render.attachNewNode(dlight)

    render.clearLight()
    render.setLight(alightNP)
    render.setLight(dlightNP)

    # Input
    self.accept('escape', self.doExit)
    self.accept('f1', self.toggleWireframe)
    self.accept('f2', self.toggleTexture)
    self.accept('f3', self.toggleDebug)
    self.accept('f5', self.doScreenshot)

    inputState.watchWithModifiers('forward', 'z')
    inputState.watchWithModifiers('reverse', 's')
    inputState.watchWithModifiers('turnLeft', 'q')
    inputState.watchWithModifiers('turnRight', 'd')
    inputState.watchWithModifiers('up', 'r')
    inputState.watchWithModifiers('down', 'f')
    inputState.watchWithModifiers('changeCam', 'c')

    # Physics
    self.setup_world()

  # _____HANDLER_____

  def doExit(self):
    self.cleanup()
    sys.exit(1)

  def cleanup(self):
    self.world = None
    self.worldNP.removeNode()

  def doReset(self, vehis):
    self.cleanup()
    self.setup_world()
    self.setup_vehicles(vehis)

  def toggleWireframe(self):
    base.toggleWireframe()

  def toggleTexture(self):
    base.toggleTexture()

  def toggleDebug(self):
    if self.debugNP.isHidden():
      self.debugNP.show()
    else:
      self.debugNP.hide()

  def doScreenshot(self):
    base.screenshot('Bullet')

  def setup_world(self):
    
    self.worldNP = render.attachNewNode('World')

    # World
    self.debugNP = self.worldNP.attachNewNode(BulletDebugNode('Debug'))
    self.debugNP.show()

    self.world = BulletWorld()
    self.world.setGravity(Vec3(0, 0, -9.81))
    self.world.setDebugNode(self.debugNP.node())
    self.world.setGroupCollisionFlag(0, 0, False)
    self.world.setGroupCollisionFlag(0, 1, True)
    self.world.setGroupCollisionFlag(1, 1, False)

    # Heightfield (static)
    height = 8.0

    img = PNMImage(Filename('data/elevation_cust4.png'))
    shape = BulletHeightfieldShape(img, height, ZUp)
    shape.setUseDiamondSubdivision(True)

    np = self.worldNP.attachNewNode(BulletRigidBodyNode('Heightfield'))
    np.node().addShape(shape)
    np.setPos(0, 0, 0)
    np.setCollideMask(BitMask32.bit(0))

    self.world.attachRigidBody(np.node())

    self.hf = np.node() # To enable/disable debug visualisation

    self.terrain = GeoMipTerrain('terrain')
    self.terrain.setHeightfield(img)
 
    self.terrain.setBlockSize(32)
    self.terrain.setNear(50)
    self.terrain.setFar(100)
    self.terrain.setFocalPoint(base.camera)
 
    rootNP = self.terrain.getRoot()
    rootNP.reparentTo(render)
    rootNP.setSz(8.0)

    offset = img.getXSize() / 2.0 - 0.5
    rootNP.setPos(-offset, -offset, -height / 2.0)
 
    self.terrain.generate()


  def setup_vehicles(self, vehis):
    self.vehicle = []

    # Steering info
    self.steering = []          # degree
    self.steeringIncrement = 120.0 # degree per second

    for h in range (len(vehis)):
      self.steering.append(0.0)
      # Chassis
      shape = BulletBoxShape(Vec3(vehis[h].dimensions_chassis[0], vehis[h].dimensions_chassis[1], vehis[h].dimensions_chassis[2]))
      ts = TransformState.makePos(Point3(0, 0, 0.5))

      new_npV = self.worldNP.attachNewNode(BulletRigidBodyNode('Vehicle'+str(h+1)))

      new_npV.node().addShape(shape, ts)
      new_npV.setPos(-45, -45, 1)
      new_npV.node().setMass(vehis[h].masse)
      new_npV.node().setDeactivationEnabled(False)
      new_npV.setCollideMask(BitMask32.bit(1))

      self.world.attachRigidBody(new_npV.node())

      # Vehicle
      self.vehicle.append(BulletVehicle(self.world, new_npV.node()))
      self.vehicle[h].setCoordinateSystem(ZUp)
      self.world.attachVehicle(self.vehicle[h])

      #self.yugoNP = loader.loadModel('data/voiture/yugo.egg')
      #self.yugoNP.reparentTo(new_npV)

      # Wheels
      for i in range (vehis[h].nombre_roues):
        #np = loader.loadModel('data/voiture/yugotireR.egg')
        #np.reparentTo(self.worldNP)
        self.addWheel(Point3(vehis[h].tableau_roues[i].position[0],
                            vehis[h].tableau_roues[i].position[1],
                            vehis[h].tableau_roues[i].position[2]), vehis[h].tableau_roues[i], h)

      self.npV.append(new_npV)

  def addWheel(self, pos, roux, h): 
    wheel = self.vehicle[h].createWheel()
    #wheel.setNode(np.node())
    wheel.setChassisConnectionPointCs(pos)
    wheel.setFrontWheel(False)

    wheel.setWheelDirectionCs(Vec3(0, 0, -1))
    wheel.setWheelAxleCs(Vec3(1, 0, 0))
    wheel.setWheelRadius(roux.rayon)
    wheel.setMaxSuspensionTravelCm(40.0)

    wheel.setSuspensionStiffness(40.0)
    wheel.setWheelsDampingRelaxation(2.3)
    wheel.setWheelsDampingCompression(4.4)
    wheel.setFrictionSlip(roux.friction)
    wheel.setRollInfluence(0.0)

  # ____TASK___

  def autoHandler(self, vehis, step):
    for h in range (len(self.vehicle)):
      self.steering[h] = vehis[h].angle_max * sin(0.1*step)
      for i in range(vehis[h].nombre_roues):
        self.vehicle[h].applyEngineForce(vehis[h].engine_force, i)
        if (vehis[h].tableau_roues[i].peut_tourner):
          self.vehicle[h].setSteeringValue(self.steering[h], i)

  def update(self, vehis, step):
    self.processInput()
    self.autoHandler(vehis, step)
    self.world.doPhysics(1/TAUX_RAFRAICHISSEMENT, 10, 1/TAUX_RAFRAICHISSEMENT)

    return Task.cont


  def processInput(self):
    camX = base.cam.getX()
    camY = base.cam.getY()
    camZ = base.cam.getZ()
    vehiX = self.npV[0].getX()
    vehiY = self.npV[0].getY()
    vehiZ = self.npV[0].getZ()
    
    if inputState.isSet('forward'):
      if(camX < vehiX):
        base.cam.setX(camX+1)
      else:
        base.cam.setX(camX-1)

      if(camY < vehiY):
        base.cam.setY(camY+1)
      else:
        base.cam.setY(camY-1)

      if(camZ < vehiZ):
        base.cam.setZ(camZ+1)
      else:
        base.cam.setZ(camZ-1)

    if inputState.isSet('reverse'):
      if(camX < vehiX):
        base.cam.setX(camX-1)
      else:
        base.cam.setX(camX+1)

      if(camY < vehiY):
        base.cam.setY(camY-1)
      else:
        base.cam.setY(camY+1)

      if(camZ < vehiZ):
        base.cam.setZ(camZ-1)
      else:
        base.cam.setZ(camZ+1)

    if inputState.isSet('turnLeft'):
      newCam = rotation(camX, camY, vehiX, vehiY, -0.1)
      base.cam.setX(newCam[0])
      base.cam.setY(newCam[1])

    if inputState.isSet('turnRight'):
      newCam = rotation(camX, camY, vehiX, vehiY, 0.1)
      base.cam.setX(newCam[0])
      base.cam.setY(newCam[1])

    if inputState.isSet('up'):
      newCam = rotation(camZ, camY, vehiZ, vehiY, 0.1)
      base.cam.setZ(newCam[0])
      base.cam.setY(newCam[1])

    if inputState.isSet('down'):
      newCam = rotation(camZ, camY, vehiZ, vehiY, -0.1)
      base.cam.setZ(newCam[0])
      base.cam.setY(newCam[1])

    if inputState.isSet('changeCam'):
      self.regarder_vehi = not self.regarder_vehi
    
    if(base.cam.getZ() < 1):
      base.cam.setZ(1)

    if(self.regarder_vehi):
      base.cam.lookAt(-50, -50, 1)
    else:
      base.cam.lookAt(0, 0, 0)

####### FIN DEF GAME ########

def simule(game, tableau_vehicules):
  nb_vehi = len(tableau_vehicules)
  game.doReset(tableau_vehicules)
  step = 0

  while (step < NB_STEPS):   #Nouvelle génération
    game.update(tableau_vehicules, step)
    if (affichage):
      taskMgr.step()
    step += 1
    for i in range (nb_vehi):
      tableau_vehicules[i].position = game.npV[i].getPos()
      tableau_vehicules[i].calcul_fitness()


###### DEBUT DEF SIMULATEUR #######
class Simulateur(Thread):
  def __init__(self, tab_vehi):
    Thread.__init__(self)
    self.game = Game()
    self.tableau_vehicules = tab_vehi

  def run(self):
    simule(self.game, self.tableau_vehicules)

###### FIN DEF SIMULATEUR ######


###### DEBUT MAIN ######
global affichage
affichage = True
threads = False
opening = 0
population = POPULATION

tab_args = gestion_args(threads, affichage, population, opening)
threads = tab_args[0]
affichage = tab_args[1]
population = tab_args[2]
opening = tab_args[3]

global numero_generation
numero_generation = 0

tableau_vehicules = generation_population(population)

numero_generation = lecture(tableau_vehicules, opening)

if(not threads):
  game = Game()

while (numero_generation < MAX_GENERATIONS):
  print('-> Génération ', numero_generation+1, '/', MAX_GENERATIONS)

  if(psutil.virtual_memory()[2] > 60):
    sys.exit("On consomme trop de mémoire, HAAAAAAAAAAAAAAAAAAA")

  if(threads):
    tab_veh = []
    tab_frids = []
    i = 0 
    while(tableau_vehicules != []):
      suppr = 20
      if(len(tableau_vehicules) < 20):
        suppr = len(tableau_vehicules)
      tab_veh.append(tableau_vehicules[:suppr])
      if(len(tableau_vehicules) <= 20):
        tableau_vehicules = []
      else:
        del tableau_vehicules[:suppr]

      new_thread = Simulateur(tab_veh[i])
      tab_frids.append(new_thread)
      i += 1

  if(threads):
    for i in range (len(tab_frids)):
      tab_frids[i].start()

    for i in range (len(tab_frids)):
      tab_frids[i].join()

    for i in range (len(tab_frids)):
      tableau_vehicules = tableau_vehicules + tab_frids[i].tableau_vehicules
  else:
    simule(game, tableau_vehicules)

  if(numero_generation % SAUVEGARDES == 0):                                                           #
    nom_fichier_vehi = 'data/generations/donnees_vehicules_' + str(numero_generation + 1) + '.csv'    #
    nom_fichier_roues = 'data/generations/donnees_roues_' + str(numero_generation + 1) + '.csv'       #
    ecriture(tableau_vehicules, nom_fichier_vehi, nom_fichier_roues)

  selection(tableau_vehicules, population)
  croisement(tableau_vehicules, population)
  mutation(tableau_vehicules, population)

  numero_generation += 1
  print()
