from random import uniform,randint,getrandbits
from math import *

from scipy.stats import truncnorm 
from statistics import mean

##### Déclaration constantes #####

MAX_GENERATIONS = 100
POPULATION = 20
PROP_REGEN = 0.1
PROP_NON_REGEN = 0.9
PROP_SELECTION = 0.4
PROP_CROISEMENT = 0.5
PROP_SANS_MUTATION = 0.2
PROBA_MUTATION = 0.7

POINT_ARRIVEE = [50, 50, 1]

DIM_MIN = [0.2, 0.2, 0.2]
DIM_MAX = [3, 3, 3]
NOMBRE_ROUES_MIN = 1
NOMBRE_ROUES_MAX = 8
ENGINE_FORCE_MIN = 100
ENGINE_FORCE_MAX = 5000

ANGLE_MIN = 0
ANGLE_MAX = 90
FRICTION_MIN = 0
FRICTION_MAX = 10000
'''
suspensions
roll_influence
'''

##### Définitions classes #####

# Début def : classe roue
class roue:
    def __init__(self, pos, dim):
        self.rayon = uniform(dim/2.0, dim)
        self.friction = randint(FRICTION_MIN, FRICTION_MAX)
        self.peut_tourner = bool(getrandbits(1))
        self.position = pos
# Fin def : classe roue

# Début def : classe véhicule
class vehicule:
    def __init__(self):
        self.fitness = 10000
        self.position = [0,0,0]
        self.dimensions_chassis = []
        self.dimensions_chassis.append(uniform(DIM_MIN[0], DIM_MAX[0]))
        self.dimensions_chassis.append(uniform(DIM_MIN[1], DIM_MAX[1]))
        self.dimensions_chassis.append(uniform(DIM_MIN[2], DIM_MAX[2]))
        self.masse = int((2*self.dimensions_chassis[0]+2*self.dimensions_chassis[1]+self.dimensions_chassis[2])*400)  #Entre 400 et 4000
        
        self.angle_max = randint(ANGLE_MIN, ANGLE_MAX)
        self.engine_force = randint(ENGINE_FORCE_MIN, ENGINE_FORCE_MAX)
        self.nombre_roues = randint(NOMBRE_ROUES_MIN, NOMBRE_ROUES_MAX)

        self.tableau_roues = []
        for i in range (self.nombre_roues):
            position = [0,0,0]
            for j in range (3):
                position[j] = uniform((-self.dimensions_chassis[j]/2),self.dimensions_chassis[j]/2)
            une_roue = roue(position, self.dimensions_chassis[2])
            self.tableau_roues.append(une_roue)

    def update_roues(self, nb_roues):
        self.tableau_roues = []
        for i in range (self.nombre_roues):
            position = [0,0,0]
            for j in range (3):
                position[j] = uniform((-self.dimensions_chassis[j]/2),self.dimensions_chassis[j]/2)
            une_roue = roue(position, self.dimensions_chassis[2])
            self.tableau_roues.append(une_roue)

    def calcul_fitness(self):
        x = self.position[0]-POINT_ARRIVEE[0]
        y = self.position[1]-POINT_ARRIVEE[1]
        z = self.position[2]-POINT_ARRIVEE[2]
        distance = sqrt(x*x+y*y+z*z)
        if(distance < self.fitness):
            self.fitness = distance

# Fin def : classe véhicule

##### Définitions fonctions : algorithme génétique #####

def generation_population(population):
    tableau = []
    for i in range (population):
        vehi = vehicule()
        tableau.append(vehi)
    return tableau


def selection(tab, population):
    tab.sort(key = lambda vehi: vehi.fitness)
    for i in range (len(tab)):
        tab[i].fitness = 10000
    for i in range(int(population*PROP_SELECTION), int(population*PROP_SELECTION+population*PROP_REGEN)):
        tab[i] = vehicule()


def croisement(tab, population):        #Prend un tableau de POPULATION*0.3 (30%) et rajoute POPULATION*0.5 (50%) fils croisés aléatoirement
    for i in range(int(population-population*PROP_CROISEMENT), population):
        tab[i] = vehicule()
        '''Choix parents'''
        papa = randint(0, int(population*PROP_SELECTION))
        maman = randint(0, int(population*PROP_SELECTION))
        while (maman == papa):
            maman = randint(0, int(population*PROP_SELECTION))
        pere = tab[papa]
        mere = tab[maman]
        '''Gestion chassis'''
        tab[i].dimensions_chassis = random_crois_pos(pere.dimensions_chassis, mere.dimensions_chassis)
        tab[i].masse = int((2*tab[i].dimensions_chassis[0]+2*tab[i].dimensions_chassis[1]+tab[i].dimensions_chassis[2])*400)
        tab[i].engine_force = random_crois_int(pere.engine_force, mere.engine_force)

        '''Nombre de roues et angles'''
        tab[i].nombre_roues = random_crois_int(pere.nombre_roues, mere.nombre_roues)
        tab[i].angle_max = random_crois_int(pere.angle_max, mere.angle_max)
        tab[i].update_roues(tab[i].nombre_roues)

        '''Gestion roues'''
        for j in range (tab[i].nombre_roues):
            parent = vehicule()
            if (pere.nombre_roues < mere.nombre_roues):
                parent = mere
            else:
                parent = pere
            if (j < min(pere.nombre_roues, mere.nombre_roues)):      #Si on peut croiser deux roues
                tab[i].tableau_roues[j].friction = random_crois_int(pere.tableau_roues[j].friction,mere.tableau_roues[j].friction)
                tab[i].tableau_roues[j].peut_tourner = mere.tableau_roues[j].peut_tourner
                for k in range (3):     #Pour x, y et z
                    tab[i].tableau_roues[j].position[k] = random_crois_int(pere.tableau_roues[j].position[k], mere.tableau_roues[j].position[k])
                    if (abs(tab[i].tableau_roues[j].position[k]) > tab[i].dimensions_chassis[k]/2):        #Si la roue dépasse du chassis à droite, haut ou devant
                        tab[i].tableau_roues[j].position[k] = tab[i].dimensions_chassis[k]/2
                    if (abs(tab[i].tableau_roues[j].position[k]) < tab[i].dimensions_chassis[k]/2):        #Si la roue dépasse du chassis à gauche, bas ou derrière
                        tab[i].tableau_roues[j].position[k] = -tab[i].dimensions_chassis[k]/2
                tab[i].tableau_roues[j].rayon = uniform(tab[i].dimensions_chassis[2]/2.0, tab[i].dimensions_chassis[2])
            else:                                                    #S'il n'y a qu'une roue à croiser
                tab[i].tableau_roues[j].friction = parent.tableau_roues[j].friction
                tab[i].tableau_roues[j].peut_tourner = parent.tableau_roues[j].peut_tourner
                for k in range (3):     #Pour x, y et z
                    tab[i].tableau_roues[j].position[k] = parent.tableau_roues[j].position[k]
                    if (tab[i].tableau_roues[j].position[k] > tab[i].dimensions_chassis[k]):        #Si la roue dépasse du chassis
                        tab[i].tableau_roues[j].position[k] = tab[i].dimensions_chassis[k]
                tab[i].tableau_roues[j].rayon = uniform(tab[i].tableau_roues[j].position[2]/2.0, tab[i].tableau_roues[j].position[2]*2)
    

def mutation(tab, population):
    for i in range (int(population*PROP_SANS_MUTATION), population):
        # dimensions
        for j in range (3):
            if (uniform(0, 1) < PROBA_MUTATION):
                tab[i].dimensions_chassis[j] = uniform(DIM_MIN[j], DIM_MAX[j])
        # actualisation masse
        tab[i].masse = int((2*tab[i].dimensions_chassis[0]+2*tab[i].dimensions_chassis[1]+tab[i].dimensions_chassis[2])*400)
        # engine_force
        if (uniform(0, 1) < PROBA_MUTATION):
            tab[i].engine_force = randint(ENGINE_FORCE_MIN, ENGINE_FORCE_MAX)
        # angle_max
        if (uniform(0, 1) < PROBA_MUTATION):
            tab[i].angle_max = randint(ANGLE_MIN, ANGLE_MAX)
        # nombre_roues
        if (uniform(0, 1) < PROBA_MUTATION):
            tab[i].nombre_roues = randint(NOMBRE_ROUES_MIN, NOMBRE_ROUES_MAX)
        # actualisation roues
        while (tab[i].nombre_roues < len(tab[i].tableau_roues)):
            tab[i].tableau_roues.pop(randint(0, len(tab[i].tableau_roues)-1))
        while (tab[i].nombre_roues > len(tab[i].tableau_roues)):
            position = [0,0,0]
            for j in range (3):
                position[j] = uniform((-tab[i].dimensions_chassis[j]/2), tab[i].dimensions_chassis[j]/2)
            une_roue = roue(position, tab[i].dimensions_chassis[2])
            tab[i].tableau_roues.append(une_roue)


###### Definitions trucs qu'on peut pas mettre dans utilitaire ####
def random_crois_int(a, b):
  r = randint(0,2)
  if(r == 0):
    return a
  elif(r == 1):
    return b
  else:
    return int(mean([a, b]))

def random_crois_pos(a, b):
  r = randint(0, 2)
  if(r == 0):
    return a
  elif(r == 1):
    return b
  else:
    c = [0, 0, 0]
    c[0] = mean([a[0], b[0]])
    c[1] = mean([a[1], b[1]])
    c[2] = mean([a[2], b[2]])
    return c