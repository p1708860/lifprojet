# Algorithme génétique : des voitures qui évoluent

Il s'agit d'un programme permettant de simuler des populations de voitures, dont le but est d'aller d'un coin à l'autre du terrain. Au fur et à mesure, les voitures sont censées évoluer et tendre vers un optimum, c'est-à-dire une voiture rapide et allant vers le point voulu.



## Getting Started

Ces instructions vont vous permettre de pouvoir lancer le programme sur votre machine par la suite en toute sérénité.

### Prérequis

Ce dont vous avez besoin pour installer le programme (ceci n'inclut pas Git ni comment faire un clone, si besoin se référer à la documentation de Git).

```
sudo apt install python3
```
Vous aurez besoin de Python3 pour faire fonctionner ce programme. C'est en effet un programme Python, qui l'eut cru ? Mais obligatoirement une des versions 3.

```
sudo apt install pip3
```
S'il ne s'est pas déjà installé avec Python3, vous aurez besoin de l'installer afin d'installer nos dépendances sur votre machine.

### Installation

Passons maintenant à l'installation de ce dont le programme a besoin pour tourner.

```
pip3 install panda3d
```
Installe la librairie *Panda3D*, le moteur graphique (incluant le moteur physique *PyBullet*).

```
pip3 install direct
```
Installe la librairie *direct* dont se sert le template que nous avons utilisé pour l'implémentation des véhicules avec le moteur physique et graphique.

## Utilisation

Il existe deux manières de lancer le programme, ceci dû à un problème de mémoire non résolu. Dans tous les cas, se rendre d'abord dans le dossier du projet via un terminal.

### La méthode traditionnelle

```
python3 graphique.py
```
Voilà la commande basique pour lancer le programme, sans aucun argument. Cela lance donc le programme avec des arguments par défaut précisés plus loin. Ci-dessous, la liste des arguments possibles, à placer après la commande ci-dessus.

* --pop **X** : permet de définir la taille d'une population à **X**. Par défaut vaut 100.

* --display **X** : permet d'activer l'affichage, et par la même désactiver le multithreading. Affiche la **X**ème partie des meilleurs d'une population (si lecture de fichier). Par défaut, l'affichage est désactivé. Si activé, **X** vaut 1*.

* --open **X** : ouvre les fichiers *data/generations/donnees_vehicules_**X**.csv* et *data/generations/donnees_roues_**X**.csv* s'ils existent afin de charger une simulation déjà faite. Par défaut, n'ouvre pas de fichier et part d'une génération aléatoire.

*  --open-name **fichier.csv** : ouvre les fichiers *data/generations/**fichier.csv*** et son fichier de roues correspondant s'ils existent afin de charger une simulation déjà faite. Par défaut, n'ouvre pas de fichier et part d'une génération aléatoire.

NB : Le **X** en argument doit être un entier.

NB* : Dans display, **X** doit être compris entre 1 et population/MAX_POP_SIMU. (ce dernier étant le nombre de véhicule par simulation (=thread), vaut 20 par défaut)

### La méthode complète

```
./run.sh
```

Lance le programme avec la méthode traditionnelle et le relance automatiquement lorsqu'il quitte pour cause de saturation mémoire.

NB : Il est donc tout à fait possible de changer les arguments comme pour la méthode traditionnelle en modifiant le contenu de *./run.sh* (ATTENTION : bien modifier les deux lignes dans ce cas !)

## Organisation des fichiers

- graphique.py : La partie graphique et le moteur physique, ainsi que le main.
- technique.py : la partie concernant l'algorithme génétique.
- utilitaire.py : la partie outil qui contient toutes sortes de fonctions utiles.

## Produit avec

* [Python3](https://www.python.org/about/) - Le langage de programmation utilisé
* [Panda3D](https://docs.panda3d.org/1.10/python/introduction/index) - Le moteur graphique de rendu 3D utilisé. Ainsi qu'un de leur template pour l'utilisation d'un véhicule avec PyBullet.
* [PyBullet](https://pybullet.org/wordpress/) - Le moteur physique utilisé, directement inclus avec Panda3D.

## Versions

Nous avons utilisé la [Forge - Univ Lyon 1](https://forge.univ-lyon1.fr/) pour gérer les versions du projet.
L'intégralité du projet peut se retrouver ici : [LIFProjet](https://forge.univ-lyon1.fr/p1708860/lifprojet).

## Auteurs

* **Charly Bollinger** - *p1708860*
* **Gaël Garcia** - *p1708760*
